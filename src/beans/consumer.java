// Chad Weirick
// This is to demo a sample object that holds firstName, lastName, and address.  It also allows REST calls to each

package beans;

import java.io.Serializable;

public class consumer implements Serializable {
	/**
	 *  Default ID made by Red Hat DS
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String firstName;
	private String lastName;
	private String address;
	private int id;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Integer getId() {
		// normally there would be a tie to a DAO in here
		return null;
	}
	
	public static Object get(Object id2) {
		// normally there would be a tie to a DAO in here
		return null;
	}
	
	
	
	
}
