package business;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import beans.consumer;

@Path("/consumer")
public class consumerService {
	
	
	@GET
	@Path("/get")
	@Produces("application/json")
	public consumer getConsumer(
	            @QueryParam("firstName") String firstName,
	            @QueryParam("lastName") String lastName,
				@QueryParam("address") String address){

		consumer thisConsumer = new consumer();
		thisConsumer.setFirstName(firstName);
		thisConsumer.setLastName(lastName);
		thisConsumer.setAddress(address);
		System.out.println("Consumer Service Called");
	    return thisConsumer;
	}
	
}



