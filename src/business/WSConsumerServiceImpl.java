package business;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import beans.consumer;

public class WSConsumerServiceImpl implements WSConsumerService {
	
	private static Map<Integer,consumer> consumers = new HashMap<Integer,consumer>();
	
	@Override
	public boolean addConsumer(consumer thisConsumer) {
		if(consumer.get(thisConsumer.getId()) != null) return false;
		consumers.put(thisConsumer.getId(), thisConsumer);
		return true;
	}

	@Override
	public boolean deleteConsumer(int id) {
		if(consumers.get(id) == null) return false;
		consumers.remove(id);
		return true;
	}

	@Override
	public consumer getConsumer(int id) {
		return consumers.get(id);
	}

	@Override
	public consumer[] getAllConsumers() {
		Set<Integer> ids = consumers.keySet();
		consumer[] thisConsumerList = new consumer[ids.size()];
		int i = 0;
		for(Integer id : ids){
			thisConsumerList[i] = (consumer) consumer.get(id);
			i++;
		}
		return thisConsumerList;
	}

}
