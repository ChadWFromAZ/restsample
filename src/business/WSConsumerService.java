package business;

import beans.consumer;

public interface WSConsumerService {
	
	public boolean addConsumer(consumer thisConsumer);
	
	public boolean deleteConsumer(int id);
	
	public consumer getConsumer(int id);
	
	public consumer[] getAllConsumers();

}
